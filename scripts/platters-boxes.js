import elements from './components/elements.js'

// Switch tabs
elements.tabs.forEach(tab => tab.onclick = function() { toggleTab(this.id, this.dataset.target)});

function toggleTab(selectedTab, id) {
   elements.tabs.forEach(tab => {
      if (tab.id === selectedTab) {
         tab.classList.add('is-active')
      } else {
         if (tab.classList.contains('is-active')) {
            tab.classList.remove('is-active')
         }
      }
   })

   elements.tabContentBoxes.forEach(tab => {
      if (tab.id == id) {
         tab.classList.remove('is-hidden')
      } else {
         tab.classList.add('is-hidden')
      }
   })
}