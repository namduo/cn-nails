import elements from './components/elements.js'

elements.form.submit.addEventListener('click', () => {

  elements.successMessage.classList.add('is-hidden')
  
  const {
    name,
    phone,
    email,
    postcode,
    date,
    type,
    size,
    checkboxes,
    comments
  } = elements.form
  
  let data = {
    name: name.value,
    email: email.value,
    phone: phone.value,
    postcode: postcode.value,
    date: date.value,
    type: type.value,
    size: size.value,
  }

  let rules = {
    name: 'required',
    email: 'required|email',
    phone: 'numeric',
    postcode: 'required'
  }
  
  let validation = new Validator(data, rules, {
    "required.name": "The <strong>name</strong> field is required.",
    "required.email": "An <strong>email</strong> address is required.",
    "email.email": "Please input a valid <strong>email</strong> address.",
    "max.phone": "Please input a valid <strong>phone number</strong>",
    "numeric.phone": "Please input a valid <strong>phone number</strong>",
    "required.postcode": "A <strong>postcode</strong> is required"
  });
  
  if (validation.passes()) {
    const additionalInfo = []
    Array.from(checkboxes).forEach(checkbox => checkbox.checked ? additionalInfo.push(checkbox.value) : null)

    axios.post('https://api.emailjs.com/api/v1.0/email/send', {
      service_id: 'riversidegrazes',
      template_id: 'template_box',
      user_id: 'user_77S6Li5F0eJpEnDBJz3JT',
      template_params: {
        name: name.value,
        email: email.value,
        phone: phone.value,
        postcode: postcode.value,
        date: date.value,
        type: type.value,
        size: size.value,
        comments: comments.value,
        additionalInfo: additionalInfo.join(' | ')
      }
    })
    .then(res => {

      if (res.status === 200) {
        elements.form.comments.value = ''
        const inputArray = Array.from(document.getElementsByTagName("input"))
        inputArray.forEach(input => {
          input.value = ''
          input.checked = false
        })
        
        elements.errorMessage.classList.add('is-hidden')
        elements.successMessage.classList.remove('is-hidden')
      }
    })
    .catch(err => {
      console.log(err);
    })
    
  } else {
    let errorMessages = Object.values(validation.errors.all())
    elements.errorMessage.classList.remove('is-hidden')
    elements.errorMessage.innerHTML = errorMessages[0]
    console.log(errorMessages);
  }

})