import elements from './components/elements.js'

elements.form.submit.addEventListener('click', () => {
  
  const {
    name,
    phone,
    email,
    postcode,
    date,
    numberOfGuests,
    type,
    checkboxes,
    comments
  } = elements.form
  
  let data = {
    name: name.value,
    email: email.value,
    phone: phone.value,
    postcode: postcode.value,
    date: date.value,
    type: type.value,
    numberOfGuests: numberOfGuests.value,
  }

  let rules = {
    name: 'required',
    email: 'required|email',
    postcode: 'required',
    type: `required`,
    date: `required`,
    numberOfGuests: `required|numeric`,
  }
  
  let validation = new Validator(data, rules, {
    "required.name": "The <strong>name</strong> field is required.",
    "required.email": "An <strong>email</strong> address is required.",
    "email.email": "Please input a valid <strong>email</strong> address.",
    "required.date": "Please let us know of your <strong>event date</strong>.",
    "required.postcode": "A <strong>postcode</strong> is required",
    "required.numberOfGuests": "Please let us know <strong>how many guests</strong> you're enquiring for"
  });
  
  if (validation.passes()) {
    const additionalInfo = []
    Array.from(checkboxes).forEach(checkbox => checkbox.checked ? additionalInfo.push(checkbox.value) : null)

    axios.post('https://api.emailjs.com/api/v1.0/email/send', {
      service_id: 'riversidegrazes',
      template_id: 'template_table',
      user_id: 'user_77S6Li5F0eJpEnDBJz3JT',
      template_params: {
        name: name.value,
        email: email.value,
        phone: phone.value,
        postcode: postcode.value,
        date: date.value,
        numberOfGuests: numberOfGuests.value,
        type: type.value,
        comments: comments.value,
        additionalInfo: additionalInfo.join(' | ')
      }
    })
    .then(res => {
      if (res.status === 200) {
        elements.form.comments.value = ''
        const inputArray = Array.from(document.getElementsByTagName("input"))
        inputArray.forEach(input => {
          input.value = ''
          input.checked = false
        })

        elements.errorMessage.classList.add('is-hidden')
        elements.successMessage.classList.remove('is-hidden')
      }
    })
    .catch(err => {
      console.log(err);
    })
    
  } else {
    let errorMessages = Object.values(validation.errors.all())
    elements.errorMessage.classList.remove('is-hidden')
    elements.errorMessage.innerHTML = errorMessages[0]
    console.log(errorMessages);
  }
})