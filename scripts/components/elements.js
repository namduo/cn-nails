const elements = {
  burger: document.querySelector('.burger'),
  nav: document.querySelector('.navbar-menu'),
  footer: document.querySelector('#footer'),
  tabs: document.querySelectorAll('.tab-list'),
  tabContentBoxes: document.querySelectorAll('.tab-pane'),
  errorMessage: document.querySelector('#message-error'),
  successMessage: document.querySelector('#message-success'),
  form: {
    submit: document.querySelector('#form-submit'),
    name: document.querySelector('#form-name'),
    phone: document.querySelector('#form-phone'),
    email: document.querySelector('#form-email'),
    date: document.querySelector('#form-date'),
    postcode: document.querySelector('#form-postcode'),
    numberOfGuests: document.querySelector('#form-number-of-guests'),
    type: document.querySelector('#form-type'),
    size: document.querySelector('#form-size'),
    checkboxes: document.querySelectorAll('input[type="checkbox"]'),
    comments: document.querySelector('#form-comments')
  }

}

export default elements;